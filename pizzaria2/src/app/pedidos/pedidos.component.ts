import { Component, OnInit } from '@angular/core';
import { ApiService} from '../api.service';


@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.css']
})
export class PedidosComponent implements OnInit {

  public pedido:any ={};
  public pizzas: Array<any>=[];
  public bebidas =[];
  public listaPedidos = [];

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getPizza().subscribe((data: any) =>{
      this.pizzas = data;
    });
    this.apiService.getBebida().subscribe((data: any) => {
      this.bebidas = data;
    });
  }

  public calculaValor (){
    return parseFloat(this.pedido.bebida.Preco) + parseFloat(this.pedido.pizza.Preco);
  }

  public adicionar() {
    this.listaPedidos.push({
      bebida: this.pedidos.bebida,
      pizza: this.pedido.pizza,
      total:
    });
    this.pedido = {};
    console.log(this.listaPedidos);
  }
  public salvar() {
    
  }
}
