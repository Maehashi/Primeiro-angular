import { Routes, CanActivate, RouterModule } from '@angular/router';
import { FormComponent } from '../app//form/form.component';
import { ListaComponent } from './lista/lista.component';
import { BebidasListComponent } from './bebidas-list/bebidas-list.component';
import { BebidasFormComponent } from './bebidas-form/bebidas-form.component';

export const appRoutes : Routes = [
    { path: 'nova-pizza', component: FormComponent },
    { path: 'lista-pizzas', component: ListaComponent},
    { path: 'lista-bebidas', component: BebidasListComponent},
    { path: 'nova-bebida', component: BebidasFormComponent}
];

export const AppRoutes = RouterModule.forRoot(appRoutes, { useHash: true });