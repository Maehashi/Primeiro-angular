import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';

import { AppRoutes } from './app.routes';

import { ApiService } from './api.service';
import { ListaComponent } from './lista/lista.component';
import { BebidasListComponent } from './bebidas-list/bebidas-list.component';
import { BebidasFormComponent } from './bebidas-form/bebidas-form.component';
import { PedidosComponent } from './pedidos/pedidos.component';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    ListaComponent,
    BebidasListComponent,
    BebidasFormComponent,
    PedidosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutes,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
