import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service'
import { Router } from '@angular/router';

@Component({
	selector: 'app-bebidas-list',
	templateUrl: './bebidas-list.component.html',
	styleUrls: ['./bebidas-list.component.css']
})
export class BebidasListComponent implements OnInit {

	public lista: Array<any> = [];

	constructor(
		private apiService: ApiService,
		private router: Router
	) { }

	ngOnInit() {
		this.apiService.getBebidas().subscribe((data : Array<any>) => {
			this.lista = data;

			console.log(this.lista);
		});
	}

	remover(bebidaId) {
		this.apiService.removerBebidas(bebidaId).subscribe(data => {
			console.log("removeu");
			location.reload();
		}, error => {
			console.log(error);
		})
	}
}