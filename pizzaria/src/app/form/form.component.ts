import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public pizza = {};

  constructor(
    private apiService: ApiService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  salvar() {
    this.apiService.inseriPizza(this.pizza).subscribe(data => {
      console.log("inseriu");
      this.router.navigate(['/lista-pizzas']);
    }, error => {
      console.log("error " + error);
    });
  }
}
