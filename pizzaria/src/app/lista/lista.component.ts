import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {

  public pizzas: any = [];

  constructor(
    private apiService: ApiService,
    private router: Router
  ) { }

  ngOnInit() {
    this.apiService.getPizza().subscribe(data => {
      this.pizzas = data;
    }, error => {
      console.log(error);
    })
  }

  remover(pizzaId) {
    this.apiService.removerPizza(pizzaId).subscribe(data => {
      console.log("removeu");
      location.reload();
    }, error => {
      console.log(error);
    })
  }
}
