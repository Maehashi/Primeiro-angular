import { Routes, CanActivate, RouterModule } from '@angular/router';
import { FormComponent } from '../app//form/form.component';
import { ListaComponent } from './lista/lista.component';
import { BebidasListComponent} from './bebidas-list/bebidas-list.component';
import { BebidasFormComponent} from './bebidas-form/bebidas-form.component';



export const appRoutes : Routes = [
    { path: 'pizza', component: FormComponent },
    { path: 'lista-pizza', component: ListaComponent},
    { path: 'lista-bebida', component: BebidasListComponent},
    { path: 'form-bebida', component: BebidasFormComponent},
    {path: 'pedidos', component: PedidosComponent}
];

export const AppRoutes = RouterModule.forRoot(appRoutes, { useHash: true });