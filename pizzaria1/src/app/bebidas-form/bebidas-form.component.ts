import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bebidas-form',
  templateUrl: './bebidas-form.component.html',
  styleUrls: ['./bebidas-form.component.css']
})
export class BebidasFormComponent implements OnInit {

  public bebida = {};

  constructor(
    private apiService: ApiService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  salvar() {
    this.apiService.inseriBebida(this.bebida).subscribe(data => {
      console.log("Deu certo inseriu");
      this.router.navigate(['/lista-bebida']);
    }, error => {
      console.log("error " + error);
    });
  }
}
