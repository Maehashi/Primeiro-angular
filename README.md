
<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

# Introdução

Após o Backend ser testado na aula anterior, agora vamos construir uma interface WEB, para listar as PIZZAS e Inserir novas pizzas no Cardápio
O desenvolvimento Frontend exige alguma infra já estabelicida na [Aula 01](https://gitlab.com/nallaworks/Pi_IoT_2_Aula01/)

>O front será em angular e o objetivo é que seja possível de uma forma amigável listar o cardápio de Pizzas, cadastrar novas Pizzas no Cardápio e remover Pizzas do Cardápio.

# O que é o Angular?

A premissa do Angular é ser um Framework MV* (Model – View – Qualquer Coisa) para desenvolvimento do front-end de aplicações web, ou seja, que rodam dentro do navegador do cliente.

Sua filosofia parte de que uma programação declarativa é muito mais importante que uma programação imperativa quando se trata de desenvolvimento web. Ele atinge isso estendendo o HTML e fazendo uma linguagem para o desenvolvimento de interfaces web dinâmicas.

A documentação oficial está disponível em [Angular] (https://angular.io/).

# Bootstrap

Bootstrap é o mais popular framework HTML, CSS, e JS para desenvolvimento de projetos responsivos e focados para dispositivos móveis na web. Desenvolvido por Jacob Thorton e Mark Otto, engenheiros do Twitter, a ferramenta era apenas uma tentativa de resolver incompatibilidades dentro da própria equipe.

 A ideia era otimizar o desenvolvimento de sua plataforma através da adoção de uma estrutura única. Isto reduziria inconsistências entre as diversas formas de se codificar, que variam de profissional para profissional. O negócio deu tão certo que eles perceberam o grande potencial da ferramenta e lançaram-a no GitHub, como um software livre.

 Com interface bastante amigável, o Bootstrap oferece uma enorme variedade de plugins e temas compatíveis com o framework. Além disso, possui integração com qualquer linguagem de programação. E a melhor parte, é gratuito graças ao seu código aberto, que em pouco tempo após o seu lançamento já recebeu a contribuição de inúmeros desenvolvedores de todo o planeta, tornando-se o software livre mais ativo do mundo.

 O seu [site](https://getbootstrap.com/) diz tudo: o “Bootstrap torna o desenvolvimento front-end mais rápido e fácil. Ele é feito para pessoas de todos os níveis de habilidade, dispositivos de todos os formatos e projetos de qualquer tamanho”.

# Como usar o Bootstrap

Esses dois links podem te ajudar.

[https://edsonjunior.com/compreendendo-grid-do-bootstrap/](https://edsonjunior.com/compreendendo-grid-do-bootstrap/)
[http://webdevacademy.com.br/tutoriais/bootstrap-layouts-sistema-grids/](http://webdevacademy.com.br/tutoriais/bootstrap-layouts-sistema-grids/)

# Vamos testar o sistema

Clona esse projeto acessa a pasta do exemplo:

```bash
cd pizzaria
````

# Vamos atualizar ou instalar o node

É muito importante atualizar o Nodejs, pois nosso exemplo necessita do NodeJs > 8.

## 1º Passo: Limpar o cache do npm

```bash
$ sudo npm cache clean -f
```

## 2º Passo: Instalação do pacote n (Pacote para gerenciamento do Node.JS)

```bash
$ sudo npm install -g n
```

## 3º Passo: Atualização

```bash
$ sudo n stable
```

Pronto, sua atualização foi realizada com sucesso. Para verificar basta executar o seguinte comando:

```bash
$ node -v
v9.6.1
```

## 4º Passo: Inicira o Servidor

```bash
json-server --watch db.json
```

# Agora vamos instalar o Angular

```bash
$ npm install -g @angular/cli
```

# Para verificar se tudo está funcionando vamos checar as versões

Lembre que deverá está na pasta "pizzaria", e uso comando abaixo.

```bash
$ ng --version
```
O resultado deverá ser assim:

```bash


    _                      _                 ____ _     ___
   / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
  / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
 / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
/_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
               |___/
    
Angular CLI: 1.6.8
Node: 9.6.1
OS: darwin x64
Angular: 5.2.6
... animations, common, compiler, compiler-cli, core, forms
... http, language-service, platform-browser
... platform-browser-dynamic, router

@angular/cli: 1.6.8
@angular-devkit/build-optimizer: 0.0.42
@angular-devkit/core: 0.0.29
@angular-devkit/schematics: 0.0.52
@ngtools/json-schema: 1.1.0
@ngtools/webpack: 1.9.8
@schematics/angular: 0.1.17
typescript: 2.5.3
webpack: 3.10.0

```
# Finalmente  iniciando a Aplicação

Na pasta da aplicação, vamos instalar as dependências.

```bash
$ npm install
```

Tudo instalado vamos, executar a Aplicação

```bash
$ ng serve
```

Se tudo deu certo, pronto:

É só acessar no browser.

```bash
** NG Live Development Server is listening on localhost:4201, open your browser on http://localhost:4201/ **
Date: 2018-02-23T14:00:09.572Z                                                          
Hash: bc5c94636cd9dae5422a
Time: 8245ms
chunk {inline} inline.bundle.js (inline) 5.79 kB [entry] [rendered]
chunk {main} main.bundle.js (main) 43.6 kB [initial] [rendered]
chunk {polyfills} polyfills.bundle.js (polyfills) 558 kB [initial] [rendered]
chunk {styles} styles.bundle.js (styles) 402 kB [initial] [rendered]
chunk {vendor} vendor.bundle.js (vendor) 9.52 MB [initial] [rendered]

webpack: Compiled successfully.
```



